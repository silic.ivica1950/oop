﻿#include <iostream>
#include <algorithm>
/*. Napisati funkciju koja prima niz brojeva i broj elemenata niza te iz niza
izbacuje duplikate, pri čemu se broj elemenata treba promijeniti. U main
funkciji korisnik unosi inicijalni broj elemenata niza. Napisati funkcije za
unos niza i ispisivanje niza.
*/

using namespace std;


void print(int* arr, int n) 
{
    for (int i = 0;i < n;i++) 
    {
        cout << "arr[" << i << "]=" << arr[i] << endl;
    }
}

void upis(int* arr, int n)
{
    for (int i = 0; i < n; i++) {
        cout << "Unesite clan: " << i << endl;
        cin >> arr[i];
    }
}


int* duplikat(int* arr, int n) {
    int* arr2 = new int[n];
    int newarrlen = 0;
    int j = 0;
    for (int i = 0;i < n;i++) {
        if (arr[i] != arr[i + 1] && i <= n - 2) {
            arr2[j] = arr[i];
            j++;
            newarrlen++;
        }
        else if (i == n - 1) {
            arr2[j] = arr[i];
            newarrlen++;
        }

    }
    cout << "newarrlen=" << newarrlen << endl;
    int *finalarray = new int[newarrlen];
    for (int i = 0;i < n;i++) {
        cout << "arr2[" << i << "]" <<" " << arr2[i] << endl;
    }
    for (int i = 0;i < newarrlen;i++) {
        finalarray[i] = arr2[i];
    }
    for (int i = 0;i < newarrlen;i++) {
        cout << "finalarray[" << i << "]" << finalarray[i] << endl;
    }
    delete[] arr2;
    return finalarray;
}

int main2() {
    int n;
    cout << "Unesite velicinu" << endl;
    cin >> n;
    int *array = new int[n];
    upis(array, n);
    print(array, n);
    sort(array, array + n);
    print(array, n);
    int *newarr = duplikat(array, n);
    delete(array);
    delete(newarr);
    return 0;
}
