﻿#include <iostream>
using namespace std;
/* Napisati funkciju koja vraća referencu na neki prvi element niza koji je veći
od nule. Koristeći povratnu vrijednost funkcije kao lvalue u main funkciji
promijenite vrijednost tog elementa na nula.
*/
int& arrzero(int* arr, int n) 
{
    for (int i = 0; i < n; i++) 
    {
        if (arr[i] > 0) 
        {
            arr[i] = 0;
            return arr[i];
        }
    }
}
int main1() 
{
    int arr[] = {0,0,3,2,1};
    cout << arr[2] << endl;
    int n = sizeof(arr) / sizeof(arr[0]);
    int lval = (arrzero(arr, n));
    cout <<" " << lval << std::endl;
    return 0;
}