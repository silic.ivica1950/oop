#include"iostream"
using namespace std;
struct MyVector
{
    int* arr;
    size_t size, capacity;

    // member functions
    void vector_new(size_t sz);
    void vector_delete();
    void vector_push_back(int n);
    void vector_pop_back();
    int& vector_front();
    int& vector_back();
    size_t vector_size();
    void print_vector();
};
void MyVector::vector_new(size_t sz)
{
    arr = new int[sz];
    capacity = sz;
}
void MyVector::vector_delete()
{
    delete[] arr;
}
void MyVector::vector_push_back(int n)
{
    if (size == capacity) {
        capacity *= 2;
        int* newarr = new int[capacity];
        {copy(arr, arr + min(vector_size(), capacity), newarr);
        delete[] arr;
        arr = newarr; }
    }
    arr[size] = n;
    size++;
    if (size == capacity) {
        capacity *= 2;
        int* newarr = new int[capacity];
        {copy(arr, arr + min(vector_size(), capacity), newarr);
        delete[] arr;
        arr = newarr; }
    }
}
void MyVector::vector_pop_back() {
    int newsize = size - 1;
    if (size == capacity / 2) {
        capacity /= 2;
        int* newarr = new int[capacity];
        copy(arr, arr + newsize, newarr);
        delete[] arr;
        arr = newarr;
        size--;
    }
    int* newarr = new int[capacity];
    copy(arr, arr + (newsize), newarr);
    delete[] arr;
    arr = newarr;
    size = newsize;
}
int& MyVector::vector_front()
{
    return arr[0];
}
int& MyVector::vector_back()
{
    return arr[(vector_size()) - 1];
}
size_t MyVector::vector_size()
{
    return size;
}

void MyVector::print_vector()
{
    for (size_t i = 0; i < vector_size(); ++i)
        cout << arr[i] << " " << std::endl;
}

int main() {
    MyVector mv;
    mv.size = 0;
    mv.vector_new(4);

    int m;
    cout << "Unesi element, Ctrl+d (linux) ili Ctrl+z (win) za kraj unosa" << endl;
    while (cin >> m) {
        mv.vector_push_back(m);
        cout << mv.size << "\t" << mv.capacity << endl;
    }
    cout << "first element " << mv.vector_front() << endl;
    cout << "last element " << mv.vector_back() << endl;
    mv.print_vector();

    cout << "removing last element" << endl;
    mv.vector_pop_back();
    mv.print_vector();
    cout << "size " << mv.vector_size() << endl;
    cout << "capacity " << mv.capacity << endl;

    mv.vector_delete();
    return 0;
}